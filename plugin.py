import * from acl

def on_use_pen(player, item):
    return []

def on_throw_pen(player, item):
    item.damage(10)

def on_collect_pen(player, item):
    player.inventory.add(item)

def on_use_book(player, item):
    read = { 'name': 'Read', handler: on_read }
    write = { 'name': 'Write', handler: on_write }
    if "Pen" in player.inventory:
        return [ read, write ]
    else:
        return [ read ]

def on_read(player, book):
    ui.show(Box(children = [
        Text(content = book.content),
        Button(content = 'Close', on_click = lambda btn: btn.parent.close())
    ]))

def on_write(player, book):
    ui.show(Box(children = [
        Input(content = book.content),
        Button(
            content = 'Done',
            on_click = save_new_page(player.inventory.find('Pen'), book)
        )
    ]))

def save_new_page(pen, book):
    def inner(btn):
        input = btn.parent.children[0]
        content = input.get_content()
        pen.damage(5)
        book.content += '\n' + content
    return inner
