# What script langage is the best?

- Gluon
- Dyon
- Rune
- Python

## Architecture

NB: this is just a proposal.

You have a `plugin.toml` file that adds the content. Some properties
may have a string value that actually refers to a function in the plugin,
by it's name, allowing to handle events related to these items/objects.

This repository contains the same plugin that adds :

- a map
- a house
- items:
  - a pen
  - a book in which you can read, and write if you have a pen

They are described in the `plugin.toml` file. Related events are
in the `plugin.{glu,dyon,rn,py}`.

Things to consider in the future:

- plugin i18n

## Some notes

- Gluon has monads, which makes it easy to tell what an event handler did,
  to save changed properties only if needed. This is not the case in Python,
  for instance, so we would need to see if there is a way to have a "hook"
  when setting a property on an object
- Partial application in Gluon allows for some nice things, especially for event handling (which is the main use case for the scripting language), see `save_new_page`
and how it is bound to the `click` event
- In some case (UI in the example plugin, for instance), Python's syntax seems easier to read, but maybe it is just because of how I designed the prototype interface (using classes in Python VS records in Gluon)
- With Dyon, if you want a FFI function with a mutable argument, you have to write the binding manually, poping and pushing from the VM's stack by yourself, etc.
